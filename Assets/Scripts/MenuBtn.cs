﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuBtn : MonoBehaviour
{
    [SerializeField] public AudioClip _startSound;

    [SerializeField] private bool _exitApp = false;
    void Awake()
    {
        if (_exitApp)
            GetComponent<Button>().onClick.AddListener(ExitApp);
        else 
            GetComponent<Button>().onClick.AddListener(LoadNextScene);
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        // Play sound
        SoundManager.PlaySound(_startSound, 1f, 1f);
    }

    private void ExitApp()
    {
        Application.Quit();
    }
}
