﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Station : MonoBehaviour
{
    private float _repairPercentage = default;
    private int _stationHP = default;
    public float RepairPercentage => _repairPercentage;
    public int StationHP => _stationHP;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        _stationHP = 100;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        _repairPercentage += 0.67f * Time.deltaTime;

        if (_repairPercentage >= 100)
        {
            _repairPercentage = 0;
            _stationHP = 100;
        }

        if (_stationHP <= 0)
        {
            SceneManager.LoadScene(2);
        }
    }

    /// <summary>
    /// Method to be used when an enemy entity strikes the station
    /// </summary>
    /// <param name="dmg"> Damage value </param>
    private void TakeHit(int dmg)
    {
        _stationHP -= dmg;
    }

    /// <summary>
    /// Method to be used when player strikes an enemy entity that can speed up
    /// the repair time
    /// </summary>
    /// <param name="repairValue"></param>
    public void SpeedRepair(int repairValue)
    {
        _repairPercentage += repairValue;
    }

    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("SmallAsteroid"))
        {
            Asteroid astro;
            if(other.TryGetComponent<Asteroid>(out astro))
                TakeHit(astro.Dmg);
            Destroy(other.gameObject);
        }
        
        if (other.CompareTag("BigAsteroid"))
        {
            Asteroid astro;
            if(other.TryGetComponent<Asteroid>(out astro))
                TakeHit(astro.Dmg);
            Destroy(other.gameObject);
        }
    }
}
