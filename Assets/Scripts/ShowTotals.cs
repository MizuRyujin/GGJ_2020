﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class ShowTotals : MonoBehaviour
{
    [SerializeField] private TotalScore _tScore = default;
    [SerializeField] private Text _scoreText = default;

    [SerializeField] private GameObject _continueText = default;

    [SerializeField] private float _textSpeed;
    
    private WaitForSeconds textWait;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        _tScore.CalcTotal();
        textWait = new WaitForSeconds(_textSpeed);
        StartCoroutine(Wait());
        _scoreText.text = $"YOUR SCORE WAS: {_tScore.TotScore}";
    }

    private IEnumerator FlashText()
    {
        yield return textWait;
        while(true)
        {
            _continueText.SetActive(!_continueText.activeSelf);
            yield return textWait;
        }
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);
        StartCoroutine(FlashText());
        while(true)
        {
            if (Input.anyKeyDown)
            {
                SceneManager.LoadScene(0);
            }
            yield return null;
        }
    }
}
