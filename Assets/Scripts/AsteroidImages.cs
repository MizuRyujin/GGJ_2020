﻿using UnityEngine;

[CreateAssetMenu(menuName = "AssHemoroids Images")]
public class AsteroidImages : ScriptableObject
{
    [SerializeField] private Sprite[] _bigAss;
    [SerializeField] private Sprite[] _smallAss;

    public Sprite[] BigAsteroids => _bigAss;
    public Sprite[] SmallAsteroids => _smallAss;
}
