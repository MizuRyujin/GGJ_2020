﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelUI : MonoBehaviour
{
    [SerializeField] private Text _scoreText = default;
    [SerializeField] private Text _hpText = default;
    [SerializeField] private TotalScore _tScore = default;
    private float _levelScore = default;
    private int _actualLevel = default;
    private GameObject _station = default;
    private GameObject _spawner = default;
    private Station _sScript = default;
    private Spawner _spawnScript = default;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        _station = GameObject.Find("Station");
        _spawner = GameObject.Find("Spawner");
        _sScript = _station.GetComponent<Station>();
        _spawnScript = _spawner.GetComponent<Spawner>();
        _actualLevel = 1;
        _tScore.Awake();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    private void Update()
    {
        _levelScore += 15 * Time.deltaTime;
        _scoreText.text = _levelScore.ToString("0");
        _hpText.text = $"Station's HP: {_sScript.StationHP}";

        if (_sScript.RepairPercentage >= 99.99)
        {
            _tScore.SaveScore(_levelScore);
            _spawnScript.IncreaseSpawnRate();
            _actualLevel += 1;
        }

        if(_sScript.StationHP <= 0)
        {
            _tScore.SaveScore(_levelScore);
        }

        if (_actualLevel == 10)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
