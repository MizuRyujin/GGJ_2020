﻿using UnityEngine;
using System.Collections;
using System;
public class Movement : MonoBehaviour
{
    [SerializeField] private float _speed = 1.5f;
    [SerializeField] private float _speedMultiplier = 3.0f;
    [SerializeField] private float _rotMultiplier = 5.0f;
    [SerializeField] private float _rotationSpeed = 1.0f;
    [SerializeField] private bool _axis = false;

    private Action _controlBehaviour;
    private float _currentSpeed;
    void Awake()
    {
        if (!_axis)
        {
            _controlBehaviour += CaptureRotation;
        }
        else
        {
            _controlBehaviour += CaptureRotationVertical;
        }
    }

    void Update()
    {
        _controlBehaviour?.Invoke();
        ConstantMoveRight();
    }

    private void CaptureRotation()
    {
        Vector2 _inputs = new Vector2
            (Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        
        float axis = _inputs.x + _inputs.y; 

        float currentRotation = _rotationSpeed * (Input.GetButton("Fire1") ? _rotMultiplier : 1);
        transform.Rotate(0.0f, 0.0f, axis * _rotationSpeed * Time.deltaTime);
    }

    private void CaptureRotationVertical()
    {
        Vector2 _inputs = new Vector2
            (Input.GetAxis("Vertical"), 0.0f);

        float axis = _inputs.x + _inputs.y;

        transform.Rotate(0.0f, 0.0f, axis * _rotationSpeed * Time.deltaTime);
    }

    private void ConstantMoveRight()
    {
        _currentSpeed = _speed * (Input.GetButton("Fire1") ? _speedMultiplier : 1);
        transform.position += transform.right * Time.deltaTime * _currentSpeed;
    }
}
