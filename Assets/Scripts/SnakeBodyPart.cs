﻿using UnityEngine;

public class SnakeBodyPart : MonoBehaviour
{
    private Snake _snakeCtrl;
    private int _partHealth = 3;
    private bool _inv;

    void Start()
    {
        _snakeCtrl = GetComponentInParent<Snake>();
    }
    public bool Invincible {get; set;}
    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        Asteroid a;
        if(other.TryGetComponent<Asteroid>(out a))
        {
            if (a.Size == AsteroidSize.Big)
            {
                _snakeCtrl.AddNewPart();
            }
            else
            {
                _partHealth--;
            }
            a.DestroyAssteroid();
            UpdateHealth();
        }
    }

    private void UpdateHealth()
    {
        if (_partHealth <= 0)
        {
            _snakeCtrl.RemovePart(gameObject);
            _snakeCtrl.UpdateSnake();
            Destroy(gameObject);
        }
    }
}
