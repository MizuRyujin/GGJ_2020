﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public enum AsteroidSize
{
    Small,
    Big,
}

public class Asteroid : MonoBehaviour
{
    [SerializeField] private AsteroidSize size = default;
    [SerializeField] private AsteroidImages sprites = default;
    [SerializeField] private GameObject[] bigParticles;
    [SerializeField] private GameObject[] smallParticles;
    [SerializeField] private bool randomAsteroid = false;

    public AsteroidSize Size => size;
    private int _dmg = default;
    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D rb;
    public int Dmg => _dmg;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        size = UnityEngine.Random.value < .9f ? size = AsteroidSize.Small : size = AsteroidSize.Big;
 
        if (size == AsteroidSize.Small)
        {
            _dmg = 10;
            gameObject.tag = "SmallAsteroid";
            PickSprite(sprites.SmallAsteroids);
        }
        else if (size == AsteroidSize.Big)
        {
            _dmg = 30;
            gameObject.tag = "BigAsteroid";
            PickSprite(sprites.BigAsteroids);
        }

    }

    private void PickSprite(Sprite[] assSprites)
    {
        Sprite renderSprite = default;

        renderSprite = assSprites[UnityEngine.Random.Range(0, assSprites.Length -1)];

        _spriteRenderer.sprite = renderSprite; 
    }

    public void DestroyAssteroid()
    {
        if (size == AsteroidSize.Big)
        {
            BigAssStuff();
        }
        else
            SmallAssStuff();
    }

    private void BigAssStuff()
    {
        int amountToSplit = UnityEngine.Random.Range(0, 4);
        for(int i = 0; i < amountToSplit; i++)
        {
            GameObject newAsteroid = Instantiate(bigParticles[0], transform);
        }
        DestroyAsteroidGameObj();
    }

    private void SmallAssStuff()
    {
        DestroyAsteroidGameObj();
    }

    private void DestroyAsteroidGameObj()
    {
        Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        ContactPoint2D contact = other.GetContact(0);
        rb.AddForce(contact.normal * contact.normalImpulse);
        StartCoroutine(LerpDrag());
    }

    private IEnumerator LerpDrag()
    {
        float ogDrag = rb.drag;
        float drag = rb.drag;
        while(drag <= ogDrag * 2)
        {
            drag += Time.deltaTime;
            rb.drag = drag;
            yield return null;
        }
        rb.drag = ogDrag;
    }

}
