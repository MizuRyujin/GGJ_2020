﻿using UnityEngine;

public class BackgroundSong : MonoBehaviour
{
    [SerializeField] private AudioClip _toLoopMusic;
    private AudioSource _source;

    private float _currentTime = 0;
    private const float _timeToWait = 48.074f; // 49.074f

    // Start is called before the first frame update
    private void Awake()
    {
        _source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void Update()
    {
        PlayOtherSong();
    }

    private void PlayOtherSong()
    {
        if (_source.loop == false)
        {
            _currentTime += 1 * Time.deltaTime;

            // CountDown seconds
            if (_currentTime >= _timeToWait)
            {
                // play and loop
                _source.clip = _toLoopMusic;
                _source.Play();
                _source.loop = true;
            }
        }
    }
}
