﻿using UnityEngine;

public class OnCollisionSound : MonoBehaviour
{
    [SerializeField] public AudioClip _colideFlop;

    private void Awake()
    {
    }

    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        SoundManager.PlaySound(_colideFlop, 1f, 1f);
    }
}
