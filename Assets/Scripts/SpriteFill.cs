﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFill : MonoBehaviour
{
    [SerializeField] private SpriteRenderer fillSprite;
    [SerializeField] private Color startColor;
    [SerializeField] private Color finalColor;
    [SerializeField] private float fillAmount;

    private Station repairValue;

    void Start()
    {
        repairValue = GameObject.FindWithTag("Station").GetComponent<Station>();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        fillAmount = (float)(repairValue.RepairPercentage / 100);
        Fill(fillAmount);
    }

    public void Fill(float fillValue)
    {
        fillSprite.transform.localScale = new Vector3
            (1, Mathf.Lerp(0.1f, 2.0f, fillValue), 1);
        fillSprite.color = ColorPercentage(fillValue);
    }
    
    private Color ColorPercentage(float amount)
    {
        return Vector4.Lerp(startColor, finalColor, amount);
    }
}
