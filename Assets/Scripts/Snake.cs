﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
    private const float _PARTS_OFFSET = .20f;

    [SerializeField] private byte _snakeLength = 8;
    [SerializeField] private float _partSpeed = 1.0f;
    [SerializeField] private Transform _head;

    [SerializeField] private GameObject _snakeBody = null;
    [SerializeField] private Sprite[] _snakeBodySprites = null;
    [SerializeField] private GameObject _snakeTail = null; 
    [SerializeField] private Sprite[] _snakeTailSprites = null; 

    private GameObject _tail;

    private int _orderInLayer;

    private List<GameObject> _snakeCollection;

    private Vector3 NextPartPosition
    {
        get
        {
            Vector3 newPartPos = transform.position;
            newPartPos.x -= _PARTS_OFFSET * _snakeCollection.Count;
            return newPartPos;
        }
    }

    private void Awake() 
    {
        _orderInLayer = 100;
        _snakeCollection = new List<GameObject>();
        _head.GetComponentInChildren<SpriteRenderer>().sortingOrder = _orderInLayer;
        SpawnBodyParts();
    }

    void Update()
    {
        UpdatePartRotation();
    }

    private void SpawnBodyParts()
    {
        GameObject sr = Instantiate
            (_snakeBody, NextPartPosition, Quaternion.identity, transform);
        
        _snakeCollection.Add(sr);
        SnakeBodyPart sb = sr.GetComponent<SnakeBodyPart>();
        sb.Invincible = false;

        SpriteRenderer actualSr =
        sr.GetComponent<SpriteRenderer>();
        actualSr.sortingOrder = --_orderInLayer;
        actualSr.sprite = _snakeBodySprites[Random.Range(0, _snakeBodySprites.Length)];

        if (_snakeCollection.Count < _snakeLength)
            SpawnBodyParts();
        else 
            SpawnTail();
    }
    
    private void SpawnTail()
    {
        _tail = Instantiate
            (_snakeTail, NextPartPosition, Quaternion.identity, transform);
        
        SnakeBodyPart sb = _tail.GetComponent<SnakeBodyPart>();
        sb.Invincible = true;

        _tail.GetComponent<SpriteRenderer>().sortingOrder = --_orderInLayer;
        _tail.GetComponent<SpriteRenderer>().sprite = _snakeTailSprites[Random.Range(0, _snakeTailSprites.Length)];
    }

    public void AddNewPart()
    {
        _snakeLength++;
        UpdateSnake();
    }
    
    public void UpdateSnake()
    {
        List<Vector3> bodyPosition = new List<Vector3>(_snakeLength);
        foreach(GameObject obj in _snakeCollection)
        {
            bodyPosition.Add(obj.transform.localPosition);
            Destroy(obj);
        }
        Destroy(_tail);

        _snakeCollection.Clear();
        SpawnBodyParts();
        for(int i = 0; i < _snakeLength - 1; i++)
        {
            _snakeCollection[i].transform.localPosition = bodyPosition[i];
        }
    }

    public void RemovePart(GameObject part)
    {
        _snakeCollection.Remove(part);
        _snakeLength--;
        UpdateSnake();
    }

    private void UpdatePartRotation()
    {
        GameObject segment;
        Vector3 tempRot;
        Vector3 positionS;
        Vector3 targetS;
        Vector3 diff;
        for (int i = 0; i < _snakeCollection.Count; i++)
        {
            segment = _snakeCollection[i];
            positionS = _snakeCollection[i].transform.position;
            targetS = i == 0 ? _head.position : _snakeCollection[i - 1].transform.position;

            _snakeCollection[i].transform.rotation = Quaternion.LookRotation(Vector3.forward, (targetS - positionS).normalized);
            
            tempRot = _snakeCollection[i].transform.rotation.eulerAngles;
            tempRot.Set(tempRot.x, tempRot.y, tempRot.z + 90);

            _snakeCollection[i].transform.rotation = Quaternion.Euler(tempRot);
            diff = positionS - targetS;
            diff.Normalize();

            if (i == 0)
                segment.transform.position = targetS;
            else
                segment.transform.position = targetS + _PARTS_OFFSET * diff;
        }

        segment = _tail;
        positionS = _tail.transform.position;
        targetS = _snakeCollection[_snakeCollection.Count - 1].transform.position;

        _tail.transform.rotation = Quaternion.LookRotation(Vector3.forward, (targetS - positionS).normalized);
            
        tempRot = _tail.transform.rotation.eulerAngles;
        tempRot.Set(tempRot.x, tempRot.y, tempRot.z + 90);

        _tail.transform.rotation = Quaternion.Euler(tempRot);
        diff = positionS - targetS;
        diff.Normalize();

        segment.transform.position = targetS + _PARTS_OFFSET * diff;
    }
}
