﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    [SerializeField] private float _spawnRange = 10f;
    [SerializeField] private int _groupSize = 2;
    [SerializeField] private Asteroid _asteroidPrefab = default;
    [SerializeField] private float _maxDistanceBetweenGroupMembers;
    [SerializeField] private float _spawnRate = default;

    private List<Asteroid> _lastGroup;
    private float _previousAngle = default;
    private float _distRad = default;

    private void Awake()
    {
        _lastGroup = new List<Asteroid>(_groupSize);
        _distRad = _maxDistanceBetweenGroupMembers * Mathf.PI / 180;
        _spawnRate = 4f;
        StartCoroutine(SpawnRate());
    }

    private void SpawnAsteroid()
    {
        Vector2 spawnPoint = Vector2.zero;
        _lastGroup.Clear();
        spawnPoint = RandomVector2(_lastGroup.Count >= _groupSize);
        Vector3 direction = transform.position - (Vector3)spawnPoint;
        float angle = Vector3.Angle(transform.forward, direction);
        Quaternion rotation = Quaternion.Euler(0.0f, 0.0f, angle);

        Asteroid newAsteroid = Instantiate(_asteroidPrefab, spawnPoint, rotation);
        if (_lastGroup.Count >= _groupSize)
        {
            _lastGroup.Clear();
        }
        _lastGroup.Add(newAsteroid);
    }

    private Vector2 RandomVector2(bool inGroup)
    {
        float angle = Random.value * Mathf.PI * 2;
        if (inGroup)
        {
            _previousAngle = angle;
            angle = Random.value > 0.5f ? angle - _distRad : angle + _distRad;
        }
        float x = Mathf.Cos(angle) * _spawnRange;
        float y = Mathf.Sin(angle) * _spawnRange;

        return new Vector2(x, y);
    }

    private IEnumerator SpawnRate()
    {
        SpawnAsteroid();
        yield return new WaitForSeconds(_spawnRate);
        StartCoroutine(SpawnRate());
    }

    public void IncreaseSpawnRate()
    {
        _spawnRate *= 0.75f;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, _spawnRange);
    }
}
